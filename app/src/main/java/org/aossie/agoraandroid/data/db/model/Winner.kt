package org.aossie.agoraandroid.data.db.model

data class Winner(
  var candidate: Candidate? = null,
  var score: Score? = null
)